-- MySQL Script generated by MySQL Workbench
-- Wed Aug 29 00:19:50 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema condomee
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema condomee
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `condomee` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_cs ;
USE `condomee` ;

-- -----------------------------------------------------
-- Table `condomee`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id person, PK',
  `first_name` NVARCHAR(50) NOT NULL COMMENT 'User Name',
  `last_name` NVARCHAR(50) NOT NULL COMMENT 'User Name',
  `email` NVARCHAR(255) NOT NULL,
  `password` NVARCHAR(255) NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`condominium_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`condominium_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`condominium_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`condominium_address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `street` NVARCHAR(50) NOT NULL COMMENT 'Street of the condominium address (External address)',
  `neighborhood` NVARCHAR(50) NOT NULL,
  `city` NVARCHAR(45) NOT NULL,
  `number` INT NOT NULL,
  `postal_code` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`condominium`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`condominium` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id Condominium, PK',
  `name` NVARCHAR(45) NOT NULL COMMENT 'Condominium name',
  `fk_condominium_type_id` INT NOT NULL,
  `fk_condominium_address_id` INT NOT NULL,
  PRIMARY KEY (`id`, `fk_condominium_address_id`),
  INDEX `fk_condominium_condominium_type1_idx` (`fk_condominium_type_id` ASC),
  INDEX `fk_condominium_condominium_address1_idx` (`fk_condominium_address_id` ASC),
  CONSTRAINT `fk_condominium_condominium_type1`
    FOREIGN KEY (`fk_condominium_type_id`)
    REFERENCES `condomee`.`condominium_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_condominium_condominium_address1`
    FOREIGN KEY (`fk_condominium_address_id`)
    REFERENCES `condomee`.`condominium_address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`profile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`profile` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Id profile, PK',
  `description` NVARCHAR(45) NOT NULL COMMENT 'Description (morador, sindico, funcionario, etc.)',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `description_UNIQUE` (`description` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`internal_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`internal_address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `buiding` NVARCHAR(45) NULL COMMENT 'Bloco',
  `apartament` INT NULL,
  `avenue` NVARCHAR(45) NULL,
  `number` INT NULL,
  `fk_condominium_id` INT NOT NULL,
  PRIMARY KEY (`id`, `fk_condominium_id`),
  INDEX `fk_internal_address_condominium1_idx` (`fk_condominium_id` ASC),
  CONSTRAINT `fk_internal_address_condominium1`
    FOREIGN KEY (`fk_condominium_id`)
    REFERENCES `condomee`.`condominium` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`condominium_has_person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`condominium_has_person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_condominium_id` INT NOT NULL,
  `fk_person_id` INT NOT NULL,
  `fk_profile_id` INT NOT NULL,
  `fk_internal_address_id` INT NULL,
  PRIMARY KEY (`id`, `fk_condominium_id`, `fk_person_id`, `fk_profile_id`),
  INDEX `fk_condominium_has_person_person1_idx` (`fk_person_id` ASC),
  INDEX `fk_condominium_has_person_condominium1_idx` (`fk_condominium_id` ASC),
  INDEX `fk_condominium_has_person_profile1_idx` (`fk_profile_id` ASC),
  INDEX `fk_condominium_has_person_internal_address1_idx` (`fk_internal_address_id` ASC),
  CONSTRAINT `fk_condominium_has_person_condominium1`
    FOREIGN KEY (`fk_condominium_id`)
    REFERENCES `condomee`.`condominium` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_condominium_has_person_person1`
    FOREIGN KEY (`fk_person_id`)
    REFERENCES `condomee`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_condominium_has_person_profile1`
    FOREIGN KEY (`fk_profile_id`)
    REFERENCES `condomee`.`profile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_condominium_has_person_internal_address1`
    FOREIGN KEY (`fk_internal_address_id`)
    REFERENCES `condomee`.`internal_address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`rule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`rule` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `description_UNIQUE` (`description` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`condominium_has_rules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`condominium_has_rules` (
  `fk_condominium_id` INT NOT NULL,
  `fk_rule_id` INT NOT NULL,
  PRIMARY KEY (`fk_condominium_id`, `fk_rule_id`),
  INDEX `fk_condominium_has_rules_rules1_idx` (`fk_rule_id` ASC),
  INDEX `fk_condominium_has_rules_condominium1_idx` (`fk_condominium_id` ASC),
  CONSTRAINT `fk_condominium_has_rules_condominium1`
    FOREIGN KEY (`fk_condominium_id`)
    REFERENCES `condomee`.`condominium` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_condominium_has_rules_rules1`
    FOREIGN KEY (`fk_rule_id`)
    REFERENCES `condomee`.`rule` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`outlay`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`outlay` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(45) NOT NULL,
  `value` DECIMAL(10,2) NOT NULL,
  `fk_condominium_id` INT NOT NULL COMMENT 'Gastos do condominio',
  PRIMARY KEY (`id`),
  INDEX `fk_outlay_condominium1_idx` (`fk_condominium_id` ASC),
  CONSTRAINT `fk_outlay_condominium1`
    FOREIGN KEY (`fk_condominium_id`)
    REFERENCES `condomee`.`condominium` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Gastos do Condominio';


-- -----------------------------------------------------
-- Table `condomee`.`event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(100) NOT NULL,
  `date_start` DATETIME NOT NULL,
  `date_end` DATETIME NULL,
  `fk_condominium_id` INT NOT NULL,
  INDEX `fk_events_condominium1_idx` (`fk_condominium_id` ASC),
  PRIMARY KEY (`id`, `fk_condominium_id`),
  CONSTRAINT `fk_events_condominium1`
    FOREIGN KEY (`fk_condominium_id`)
    REFERENCES `condomee`.`condominium` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`financial_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`financial_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `value` DECIMAL(6,2) NOT NULL,
  `date_ref` DATETIME NOT NULL,
  `date_payment` DATETIME NULL,
  `fk_condominium_has_person_id` INT NOT NULL,
  PRIMARY KEY (`id`, `fk_condominium_has_person_id`),
  INDEX `fk_financial_history_1_idx` (`fk_condominium_has_person_id` ASC),
  CONSTRAINT `fk_financial_history_1`
    FOREIGN KEY (`fk_condominium_has_person_id`)
    REFERENCES `condomee`.`condominium_has_person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`complaint`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`complaint` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(255) NOT NULL,
  `fk_condominium_has_person_id` INT NOT NULL,
  PRIMARY KEY (`id`, `fk_condominium_has_person_id`),
  INDEX `fk_complaint_1_idx` (`fk_condominium_has_person_id` ASC),
  CONSTRAINT `fk_complaint_1`
    FOREIGN KEY (`fk_condominium_has_person_id`)
    REFERENCES `condomee`.`condominium_has_person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`survey`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`survey` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  `date_start` DATETIME NOT NULL,
  `date_end` DATETIME NULL,
  `completed` TINYINT(1) NOT NULL,
  `fk_condominium_has_person_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_survey_condominium_has_person1_idx` (`fk_condominium_has_person_id` ASC),
  CONSTRAINT `fk_survey_condominium_has_person1`
    FOREIGN KEY (`fk_condominium_has_person_id`)
    REFERENCES `condomee`.`condominium_has_person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`question_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`question_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `description_UNIQUE` (`description` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`question` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(45) NOT NULL,
  `fk_survey_id` INT NOT NULL,
  `fk_question_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_question_survey1_idx` (`fk_survey_id` ASC),
  INDEX `fk_question_question_type1_idx` (`fk_question_type_id` ASC),
  CONSTRAINT `fk_question_survey1`
    FOREIGN KEY (`fk_survey_id`)
    REFERENCES `condomee`.`survey` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_question_type1`
    FOREIGN KEY (`fk_question_type_id`)
    REFERENCES `condomee`.`question_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `condomee`.`answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`answer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `checked` TINYINT(1) NOT NULL,
  `fk_question_id` INT NOT NULL,
  `comment` NVARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_answer_question1_idx` (`fk_question_id` ASC),
  CONSTRAINT `fk_answer_question1`
    FOREIGN KEY (`fk_question_id`)
    REFERENCES `condomee`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Representa uma resposta.\nCaso se deseja fazer sim/nao, crie duas respostas, Sim e Nao e coloque o question_type = single\n';


-- -----------------------------------------------------
-- Table `condomee`.`answer_has_condominium_has_person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `condomee`.`answer_has_condominium_has_person` (
  `fk_answer_id` INT NOT NULL,
  `fk_condominium_has_person_id` INT NOT NULL,
  PRIMARY KEY (`fk_answer_id`, `fk_condominium_has_person_id`),
  INDEX `fk_answer_has_condominium_has_person_condominium_has_person_idx` (`fk_condominium_has_person_id` ASC),
  INDEX `fk_answer_has_condominium_has_person_answer1_idx` (`fk_answer_id` ASC),
  CONSTRAINT `fk_answer_has_condominium_has_person_answer1`
    FOREIGN KEY (`fk_answer_id`)
    REFERENCES `condomee`.`answer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_answer_has_condominium_has_person_condominium_has_person1`
    FOREIGN KEY (`fk_condominium_has_person_id`)
    REFERENCES `condomee`.`condominium_has_person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
